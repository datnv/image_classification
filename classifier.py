import numpy as np
import cv2
import tensorflow as tf
import tensorflow.contrib.image

image_size=120
crop_size=96
nbclasses=13
nbchanels=3
num_checkpoint_files=4
min_index_checkpoint_file=7

class Graph(object):
    def __init__(self,index):
        self._load_model(index)

    def _load_model(self,index):
        #graph = tf.Graph()
        #sess = tf.Session(graph=graph)
        sess=tf.InteractiveSession()
        saver = tf.train.import_meta_graph('save/model_'+str(index)+'.ckpt.meta')
        saver.restore(sess, "save/model_"+str(index)+".ckpt")
        self.x = tf.get_collection('x')[0]
        #self.keep_prob_fc1 = tf.get_collection('keep_prob_fc1')[0]
        #self.keep_prob_fc2 = tf.get_collection('keep_prob_fc2')[0]
        self.keep_prob_fc1 = tf.placeholder(tf.float32)
        self.keep_prob_fc2 = tf.placeholder(tf.float32)
        self.y_conv = tf.get_collection('y_conv')[0]
        self.is_training = tf.get_collection('is_training')[0]
        self.X_mean = np.load('file npy/X_mean.npy')
        self.X_std = np.load('file npy/X_std.npy')

    def _get_y(self,x_batch):
        y = self.y_conv.eval(
            feed_dict={self.x: x_batch
                , self.keep_prob_fc1: 1.0
                , self.keep_prob_fc2: 1.0
                , self.is_training: False})
        return y

    def get_probability_center_crop(self,path,set):
        return self._get_probability(path, set, 1)

    def get_probability_ten_crops(self,path,set):
        return self._get_probability(path, set, 10)

    def _get_probability(self,path, set, nb_crop):
        y_average = np.zeros((1, nbclasses))
        num_average = 0
        for j in range(len(set)):
            y_sub = self._get_y(_np_preprocessing_batch(set[j], self.X_mean, self.X_std))
            y_predict_tensor = tf.constant(y_sub)
            y_softmax_tensor = tf.nn.softmax(y_predict_tensor)
            y_softmax = y_softmax_tensor.eval()
            y_average += y_softmax
            num_average += 1
        y_average /= float(num_average)
        return y_average



class Classifier(object):
    def __init__(self,num_checkpoints):
        self._load_model(num_checkpoints)

    def _load_model(self, num_checkpoints):
        self.list_graph=[]
        for i in range(num_checkpoints):
            graph_index = min_index_checkpoint_file + num_checkpoint_files - 1 - i
            self.list_graph.append(Graph(graph_index))
        self.max_ng_index = np.load('file npy/max_ng_index.npy')[0]
        itc=np.load('file npy/index_to_class.npy')
        index_to_class={}
        for pair in itc:
            index_to_class[int(pair[0])]=pair[1].decode("utf-8")
        self.index_to_class=index_to_class


    def get_label_center_crop(self,path):
        return self._get_label(path, 1)

    def get_label_ten_crops(self,path):
        return self._get_label(path, 10)

    def _get_label(self,path, nb_crop):
        img = cv2.imread(path)
        img = cv2.resize(img, (image_size, image_size), interpolation=cv2.INTER_CUBIC)
        img = img[:, :, ::-1].astype(np.float32)  # convert to RGB
        x_batch=np.reshape(img,(1,image_size,image_size,nbchanels))
        set = []
        if nb_crop == 1:
            set = _np_center_crop_batch(x_batch, crop_size)
        elif nb_crop == 10:
            set = _np_ten_crop_batch(x_batch, crop_size)


        y_average = np.zeros((1, nbclasses))
        num_average = 0
        for i in range(len(self.list_graph)):
            y_softmax=[]
            if nb_crop==1:
                y_softmax = self.list_graph[i].get_probability_center_crop(path,set)
            elif nb_crop==10:
                y_softmax = self.list_graph[i].get_probability_ten_crops(path,set)
            y_average += y_softmax
            num_average += 1
        y_average /= float(num_average)

        index = np.argmax(y_average, 1)[0]
        sub_label=self.index_to_class[index]
        label=''
        if index<=self.max_ng_index:
            label='NG'
        else:
            label='OK'
        y_string=self._convert_y_to_string(y_average)

        return sub_label, label, y_string

    def _convert_y_to_string(self,y):
        y=y[0]
        result=[]
        for i in range(len(y)):
            pair=[self.index_to_class[i],round(y[i],5)]
            result.append(pair)
        return result

def _np_center_crop_batch(x_batch,crop_size):
    '''
    :param x_batch: have size (-1,width,height,3)
    :param crop_size:
    :return:
    '''
    width = np.shape(x_batch)[1]
    x = (width - crop_size) // 2
    y = (width - crop_size) // 2
    return [x_batch[:,x:x + crop_size, y:y + crop_size,:]]

def _np_center_crop_img(img,crop_size):
    '''
    :param img: have size (width,height,3)
    :param crop_size:
    :return:
    '''
    width = np.shape(img)[0]
    x = (width - crop_size) // 2
    y = (width - crop_size) // 2
    return img[x:x + crop_size, y:y + crop_size,:]

def _np_preprocessing_batch(x_batch,X_mean,X_std):
    x_batch = (x_batch - X_mean) / X_std
    return x_batch

def _np_ten_crop_batch(x_batch,crop_size):
    width = np.shape(x_batch)[1]
    x = (width - crop_size) // 2
    y = (width - crop_size) // 2
    batch_center = x_batch[:, x:x + crop_size, y:y + crop_size,:]
    batch_topleft = x_batch[:, :crop_size:, :crop_size:,:]
    batch_topright = x_batch[:, width-crop_size:width:, :crop_size:,:]
    batch_downright = x_batch[:, width-crop_size:width:, width-crop_size:width:,:]
    batch_downleft = x_batch[:, :crop_size:, width-crop_size:width:,:]
    return batch_center, batch_topleft, batch_topright, batch_downright, batch_downleft, \
           batch_center[:, :, ::-1,:], batch_topleft[:, :, ::-1,:], batch_topright[:, :, ::-1,:], batch_downright[:, :, ::-1,:], \
           batch_downleft[:, :, ::-1,:]

#classifier=Classifier()

#sub_label,label,probabilities=classifier.get_label_ten_crops('/home/dat/Downloads/2.png')

#print(sub_label)
#print(label)
#print(probabilities)